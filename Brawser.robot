***Settings***
Library  BuiltIn
Library  SeleniumLibrary
Library  OperatingSystem


***Variables***
${url}=  https://ptt-devpool-robotframework.herokuapp.com
${User}=  admin
${password}=  1234
${NFname}=  Robin
${NLname}=  Hood
${Email}=  Robin_Hood@gmail.com


***Test Cases***
1.เปิด Browser 
    Open Browser  about:blank  browser=Chrome
    Maximize Browser Window
    Set Selenium Speed  0.5
2.ไปที่ google.co.th
    Go To  ${url}

3.กรอกข้อมูลคำว่า Raptor 180
    Input Text  id=txt_username  ${User}
    Input Text  id=txt_password  ${password}
    
4.กดค้นหา
    ClickButton  id=btn_login

5.คลิ๊ก New Data
    ClickButton  id=btn_new
6.กรอกข้อมูล New Data
    Input Text  id=txt_new_firstname  ${NFname}
    Input Text  id=txt_new_lastname  ${NLname}
    Input Text  id=txt_new_email  ${Email}
7.กด Save
    ClickButton  id=btn_new_save
8.Screenshot
    Capture Page Screenshot  filename=${CURDIR}/screenshot.png
9.Close Browser
    Close All Browsers

    
***Keywords***
